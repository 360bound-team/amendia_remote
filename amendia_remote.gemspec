# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'amendia_remote/version'

Gem::Specification.new do |spec|
  spec.name          = "amendia_remote"
  spec.version       = AmendiaRemote::VERSION
  spec.authors       = ["KCh"]
  spec.email         = ["kch@list.ru"]
  spec.description   = %q{This gem helps use api from amendiatest.com}
  spec.summary       = %q{This gem helps use api from amendiatest.com}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.files = Dir["{lib}/**/*"]
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"

  spec.add_dependency "rest-client"
end
