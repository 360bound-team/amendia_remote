module AmendiaRemote
    module Api
        require 'rest_client'
        class CouldNotConnectError < StandardError; end   

        def self.authorize(email, password)
            payload = {
                'email' => email,
                'password' => password,
            }.to_json   

            remote_response payload, AmendiaRemote.config.authorize[:url], 'post'
        end

        def self.registration(email, password, api_role, first_name=nil, last_name=nil, phone=nil, recommender=nil, frontend_role=nil)
            payload = {'user' => {
                'email' => email.downcase,
                'password' => password,
                'api_role' => api_role,
                'first_name' => first_name,
                'last_name' => last_name,
                'phone' => phone,                    
                'recommended_by' => recommender,
                'frontend_role' => frontend_role,
            }}.to_json   

            remote_response payload, AmendiaRemote.config.registration[:url], 'post'
        end

        def self.request_access(email, site)
            payload = {'request_access' => {
                'email' => email.downcase,
                'site' => site,
            }}.to_json   

            remote_response payload, AmendiaRemote.config.request_access[:url], 'post'
        end

        def self.get_common_user_api_token
            response = self.authorize(
                AmendiaRemote.config.api_user[:email],
                AmendiaRemote.config.api_user[:password]
            )

            if response && response.code == '200'
                remote_user = JSON.parse(response.body)
                return remote_user['api_token']
            end                   
        end

        def self.get_product(id, api_token)            
            payload = {
                "api_token" => api_token,
            }.to_json   
            
            remote_response payload, "#{AmendiaRemote.config.product[:url]}/#{id}"
        end 

        def self.get_product_2(id, options = {})            
            make_request "#{AmendiaRemote.config.product[:url]}/#{id}", options  
        end 

        def self.get_products(category_id, api_token)
            payload = {
                "api_token" => api_token,
                'category_id' => category_id,
            }.to_json

            remote_response payload, "#{AmendiaRemote.config.product[:url]}"
        end 
        
        def self.get_product_cat(id, api_token)
            payload = {
                "api_token" => api_token,
            }.to_json
            remote_response payload, "#{AmendiaRemote.config.product_cat[:url]}/#{id}"
        end      

        def self.get_product_cat_2(id, options = {})
            make_request "#{AmendiaRemote.config.product_cat[:url]}/#{id}", options
        end      

        def self.get_instrument(id, api_token)
            payload = {
                "api_token" => api_token,
            }.to_json                  
            remote_response payload, "#{AmendiaRemote.config.instruments[:url]}/#{id}"
        end 

        def self.get_course(id, api_token)
            payload = {
                "api_token" => api_token,
            }.to_json                  
            remote_response payload, "#{AmendiaRemote.config.courses[:url]}/#{id}"
        end 

        private
            def self.make_request(url, options, method='get')
                host_url = AmendiaRemote.config.host_url

                begin
                    if method == 'get'
                        response = RestClient.get "#{host_url}#{url}", { params: options }
                    else
                        response = RestClient.post url, { params: options }
                    end
                    
                    if response && response.code == 200
                        JSON.parse(response.body)
                    else
                        Rails.logger.warn "remote_object error: #{response.code}" if response
                    end
                rescue Exception => e
                    Rails.logger.warn "AmendiaRemote remote_object error: #{e}" 
                    raise CouldNotConnectError.new
                end                 
            end

            def self.remote_response(payload, url, method='get')
                host = AmendiaRemote.config.host
                port = AmendiaRemote.config.port

                if method == 'get'
                    req = Net::HTTP::Get.new(url, initheader = {'Content-Type' =>'application/json'})
                elsif method == 'post'
                    req = Net::HTTP::Post.new(url, initheader = {'Content-Type' =>'application/json'})
                end
                req.body = payload

                begin
                    response = Net::HTTP.new(host, port).start {|http| http.request(req) }   
                rescue Exception => e
                    raise CouldNotConnectError.new, e
                    #raise CouldNotConnectError.new, message: e, error_code: AmendiaRemote::Config::COULD_NOT_CONNECT, status: 500
                end

                return response
            end          
    end

end
