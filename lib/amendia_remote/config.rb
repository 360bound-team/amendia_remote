module AmendiaRemote
	class Config		
		attr_accessor :host, :port, :host_url, :access_role, :api_user, :authorize, :registration, :request_access, :product, :product_cat, :instruments, :courses

		COULD_NOT_CONNECT = "COULD_NOT_CONNECT_TO_SERVER"

		def initialize
    		@host = 'amendiatraining.com'
    		@port = '80'
    		@host_url = 'http://amendiatraining.com'
    		@access_role = 'Blue Site User'

	        @api_user = { email: 'common@amendia.com', password: 'Z7gvBqJ002x7' }

	        @authorize = { url: '/api/v1/users/authorize', method: 'post' }
	        @registration = { url: '/api/v1/users', method: 'post' }
	        @request_access = { url: '/api/v1/users/request_access', method: 'post' }
	        @product = { url: '/api/v1/products', method: 'get' }
	        @product_cat = { url: '/api/v1/product_categories', method: 'get' }
	        @instruments = { url: '/api/v1/instruments', method: 'get' }
	        @courses = { url: '/api/v1/courses', method: 'get' }			
		end
	end
end