require "amendia_remote/version"
require "amendia_remote/config"
require "amendia_remote/api"

module AmendiaRemote
	def self.config
		@config ||= Config.new
	end

	def self.configure
		yield config
	end

	def self.authorize(email, password)
		Api.authorize(email, password)
	end	

	def self.registration(email, password, api_role, first_name=nil, last_name=nil, phone=nil, recommender=nil, frontend_role=nil)
		Api.registration(email, password, api_role, first_name, last_name, phone, recommender, frontend_role)
	end

	def self.request_access(email, site)
		Api.request_access(email, site)
	end

	def self.common_user_api_token
		Api.get_common_user_api_token
	end

	def self.product(id, api_token)
		Api.get_product(id, api_token)
	end

	def self.product_2(id, options = {})
		Api.get_product_2(id, options)
	end

	def self.products(category_id, api_token)
		Api.get_products(category_id, api_token)
	end

	def self.product_cat(id, api_token)
		Api.get_product_cat(id, api_token)
	end
	def self.product_cat_2(id, options = {})
		Api.get_product_cat_2(id, options)
	end

	def self.instrument(id, api_token)
		Api.get_instrument(id, api_token)
	end

	def self.course(id, api_token)
		Api.get_course(id, api_token)
	end	
end
