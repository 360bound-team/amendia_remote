class InitializerGenerator < Rails::Generators::NamedBase
	source_root File.expand_path('../templates', __FILE__)

	def create_initializer_file
		copy_file "amendia_remote.rb", "config/initializers/amendia_remote.rb"
	end  
end
