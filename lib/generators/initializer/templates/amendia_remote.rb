AmendiaRemote.configure do |c|
	# Server host
    c.host = 'amendiatraining.com'

    # Server port
    c.port = '80'

    # Server url
    c.host_url = 'http://amendiatraining.com'

    # Role for new user
    c.access_role = 'Site and App User'    
end
